package core;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static core.ExtentTestManager.*;
import static core.Utilities.getPropertyFromConfig;

public class TestBase {

   /* @BeforeSuite(alwaysRun = true)
    public static void setupDB() throws SQLException {
        ConnectDB.connection = getConnection("root", "root1234", "batch4");
        ConnectDB.statement = ConnectDB.connection.createStatement();
    }*/

    public static WebDriver driver = null;
    private static ExtentReports extent;
    private static String username = getPropertyFromConfig("browserstackUsername");
    private static String accessKey = getPropertyFromConfig("browserstackPassword");

    @BeforeMethod(alwaysRun = true)
    @Parameters({"browserName", "url", "os", "cloud"})
    public static WebDriver getDriver(String browserName, String url, String os, boolean cloud) {
        if (cloud) {
            DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
            desiredCapabilities.setCapability("browser", browserName);
            desiredCapabilities.setCapability("browser_version", "80");
            desiredCapabilities.setCapability("os", os);
            desiredCapabilities.setCapability("os_version", "10");
            desiredCapabilities.setCapability("resolution", "1024x768");
            desiredCapabilities.setCapability("name", "Test Cloud Execution");

            URL urlForBrowserstack = null;
            try {
                urlForBrowserstack = new URL("https://" + username + ":" + accessKey + "@hub-cloud.browserstack.com/wd/hub");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            driver = new RemoteWebDriver(urlForBrowserstack, desiredCapabilities);
        } else {
            if (os.equalsIgnoreCase("mac")) {
                if (browserName.equalsIgnoreCase("chrome")) {
                    System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
                    driver = new ChromeDriver();
                } else if (browserName.equalsIgnoreCase("firefox")) {
                    System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver");
                    driver = new FirefoxDriver();
                }
            } else if (os.equalsIgnoreCase("windows")) {
                if (browserName.equalsIgnoreCase("chrome")) {
                    System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
                    driver = new ChromeDriver();
                } else if (browserName.equalsIgnoreCase("firefox")) {
                    System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
                    driver = new FirefoxDriver();
                }
            }
        }
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(url);
        return driver;
    }

    @AfterMethod(alwaysRun = true)
    public static void quiteBrowser() {
        driver.quit();
    }

    public static void sleepFor(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static WebElement getElementFromXpath(String xpath) {
        WebElement element = driver.findElement(By.xpath(xpath));
        return element;
    }

    public static boolean isElementSelected(WebElement element) {
        boolean value = element.isSelected();
        return value;
    }

    public static boolean isElementDisplayed(WebElement element) {
        boolean value = element.isDisplayed();
        return value;
    }

    public static String getText(WebElement element) {
        String value = element.getText();
        return value;
    }

    public static void mouseHoverTo(WebElement firstWebElement, WebElement secondWebElement) {
        Actions actions = new Actions(TestBase.driver);
        actions.moveToElement(firstWebElement).perform();
        ExtentTestManager.log("mouse hovered to " + firstWebElement.getText());
        secondWebElement.click();
        ExtentTestManager.log("clicked on " + secondWebElement.getText());
    }

    public static void clickByXpath(String xpath) {
        driver.findElement(By.xpath(xpath)).click();
    }

    public static void typeByXpath(String xpath, String value) {
        driver.findElement(By.xpath(xpath)).sendKeys(value);
    }


    public static void waitForElementToBeVisible(WebElement element) {
        WebDriverWait webDriverWait = new WebDriverWait(driver, 10);
        webDriverWait.until(ExpectedConditions.visibilityOf(element));
    }

    public static void waitForElementNotToBeVisible(WebElement element) {
        WebDriverWait webDriverWait = new WebDriverWait(driver, 10);
        webDriverWait.until(ExpectedConditions.invisibilityOf(element));
    }

    public static void waitForElementToBeClickable(WebElement element) {
        WebDriverWait webDriverWait = new WebDriverWait(driver, 10);
        webDriverWait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public static void waitForElementToBeSelected(WebElement element) {
        WebDriverWait webDriverWait = new WebDriverWait(driver, 10);
        webDriverWait.until(ExpectedConditions.elementToBeSelected(element));
    }

    public static void scrollDown() {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("window.scrollBy(0,1000)");
    }

    //Extent Report Setup
    @BeforeSuite(alwaysRun = true)
    public void extentSetup(ITestContext context) {
        ExtentTestManager.setOutputDirectory(context);
        extent = ExtentTestManager.getInstance();
    }

    //Extent Report Setup for each methods
    @BeforeMethod(alwaysRun = true)
    public void startExtent(Method method) {
        String className = method.getDeclaringClass().getSimpleName();
        ExtentTestManager.startTest(method.getName());
        ExtentTestManager.getTest().assignCategory(className);
    }

    //Extent Report cleanup for each methods
    @AfterMethod(alwaysRun = true)
    public void afterEachTestMethod(ITestResult result) {
        ExtentTestManager.getTest().getTest().setStartedTime(getTime(result.getStartMillis()));
        ExtentTestManager.getTest().getTest().setEndedTime(getTime(result.getEndMillis()));
        for (String group : result.getMethod().getGroups()) {
            ExtentTestManager.getTest().assignCategory(group);
        }

        if (result.getStatus() == 1) {
            ExtentTestManager.getTest().log(LogStatus.PASS, "TEST CASE PASSED : " + result.getName());
        } else if (result.getStatus() == 2) {
            ExtentTestManager.getTest().log(LogStatus.FAIL, "TEST CASE FAILED : " + result.getName() + " :: " + getStackTrace(result.getThrowable()));
        } else if (result.getStatus() == 3) {
            ExtentTestManager.getTest().log(LogStatus.SKIP, "TEST CASE SKIPPED : " + result.getName());
        }
        ExtentTestManager.endTest();
        extent.flush();
        if (result.getStatus() == ITestResult.FAILURE) {
            captureScreenshot(driver, result.getName());
        }
    }

    //Extent Report closed
    @AfterSuite(alwaysRun = true)
    public void generateReport() {
        extent.close();
    }

}
