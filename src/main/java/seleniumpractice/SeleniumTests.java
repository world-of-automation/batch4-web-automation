package seleniumpractice;

import core.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import java.util.List;

public class SeleniumTests {

    private WebDriver driver = null;

    @Test
    public void testDragNDrop() {
        driver = TestBase.getDriver("chrome", "http://demo.guru99.com/test/drag_drop.html", "mac", false);

        WebElement elementFrom = driver.findElement(By.xpath("//li[@data-id='2'][1]"));
        WebElement elementTo = driver.findElement(By.id("amt7"));

        Actions actions = new Actions(driver);
        actions.dragAndDrop(elementFrom, elementTo).build().perform();

        driver.quit();
    }

    @Test
    public void testiFrame() {
        driver = TestBase.getDriver("chrome", "https://demoqa.com/iframe-practice-page/", "mac", false);
        TestBase.sleepFor(10);

        //-frameID -frameName -frameIndex
        driver.switchTo().frame("IF1");
        driver.findElement(By.linkText("Cucumber Tutorial")).click();

        driver.switchTo().defaultContent();
        driver.findElement(By.linkText("Interactions")).click();

        driver.quit();
    }

    @Test
    public void testScrollInsideiFrame() {
        driver = TestBase.getDriver("chrome", "https://demoqa.com/iframe-practice-page/", "mac", false);
        TestBase.sleepFor(10);

        //-frameID -frameName -frameIndex
        driver.switchTo().frame("IF1");

        TestBase.scrollDown();
        TestBase.sleepFor(5);
        driver.quit();
    }


    @Test
    public void testScrollToSpecificElement() {
        driver = TestBase.getDriver("chrome", "https://www.ebay.com/", "mac", false);

        WebElement element = driver.findElement(By.linkText("eBay Classifieds"));

        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("arguments[0].scrollIntoView(true);", element);
        TestBase.sleepFor(5);
        driver.quit();
    }


    @Test
    public void handleAlert() {
        driver = TestBase.getDriver("chrome", "http://demo.guru99.com/test/delete_customer.php", "mac", false);
        WebElement element = driver.findElement(By.xpath("//input[@name='cusid']"));
        element.sendKeys("1");

        driver.findElement(By.xpath("//input[@name='submit']")).click();
        TestBase.sleepFor(5);
        driver.switchTo().alert().dismiss();

        driver.findElement(By.xpath("//input[@name='submit']")).click();
        TestBase.sleepFor(5);
        String alertText = driver.switchTo().alert().getText();
        System.out.println(alertText);
        driver.switchTo().alert().accept();

        TestBase.sleepFor(5);
        driver.switchTo().alert().accept();
        TestBase.sleepFor(5);
        driver.quit();
    }

    @Test
    public void testRadioButton() {
        driver = TestBase.getDriver("chrome", "https://aa.com", "mac", false);

        List<WebElement> radioBtns = driver.findElements(By.xpath("//input[@type='radio']"));
        WebElement oneWay = driver.findElement(By.xpath("//label[@for='flightSearchForm.tripType.oneWay']"));
        WebElement roundTrip = driver.findElement(By.xpath("//label[@for='flightSearchForm.tripType.roundTrip']"));

        for (int i = 0; i < radioBtns.size(); i++) {
            String text = radioBtns.get(i).getAttribute("value");
            if (text.equalsIgnoreCase("oneWay")) {
                oneWay.click();
                break;
            }
        }
    }


    @Test
    public void doubleClickPractice() {
        driver = TestBase.getDriver("chrome", "http://demo.guru99.com/test/simple_context_menu.html", "mac", false);

        WebElement element = driver.findElement(By.xpath("//button[@ondblclick='myFunction()']"));
        Actions actions = new Actions(driver);
        actions.doubleClick(element).perform();
    }


    @Test
    public void rightClickPractice() {
        driver = TestBase.getDriver("chrome", "http://demo.guru99.com/test/simple_context_menu.html", "mac", false);

        WebElement element = driver.findElement(By.xpath("//span[@class='context-menu-one btn btn-neutral']"));
        Actions actions = new Actions(driver);
        actions.contextClick(element).perform();
    }
}
