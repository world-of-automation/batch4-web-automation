package seleniumbasic;

import core.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestNGIntroduction extends TestBase {

    @Test
    public void searchForJavaBooksInEbay() {
        getDriver("chrome", "https://www.ebay.com", "mac", false);
        typeByXpath("//input[@id='gh-ac']", "Java Books");
        WebElement typeBar = getElementFromXpath("//input[@id='gh-ac']");
        String valueFromUI = typeBar.getAttribute("value");
        Assert.assertEquals("Java Books", valueFromUI);
        System.out.println(valueFromUI + " is validated");
        clickByXpath("//input[@type='submit']");

        String currentTitle = driver.getTitle();
        Assert.assertTrue(currentTitle.contains("Java Books"));
        System.out.println(currentTitle + " is validated");

        quiteBrowser();
    }


    @Test
    public void clickOnNonFictionAcceptsOffers() {
        getDriver("chrome", "https://www.ebay.com", "mac", false);
        typeByXpath("//input[@id='gh-ac']", "Java Books");
        WebElement typeBar = getElementFromXpath("//input[@id='gh-ac']");
        String valueFromUI = typeBar.getAttribute("value");
        Assert.assertEquals("Java Books", valueFromUI);
        System.out.println(valueFromUI + " is validated");
        clickByXpath("//input[@type='submit']");
        driver.findElement(By.linkText("Nonfiction")).click();
        //tagname[text()='textThatYouAreExpecting']
        //tagname[contains(text(),'partialTextThatYouAreExpecting')]
        sleepFor(3);
        clickByXpath("//h2[text()='Accepts Offers']");
        sleepFor(5);
        quiteBrowser();
    }

    @Test
    public void clickOnNonFictionAuction() {
        getDriver("chrome", "https://www.ebay.com", "mac", false);
        typeByXpath("//input[@id='gh-ac']", "Java Books");
        WebElement typeBar = getElementFromXpath("//input[@id='gh-ac']");
        String valueFromUI = typeBar.getAttribute("value");
        Assert.assertEquals("Java Books", valueFromUI);
        System.out.println(valueFromUI + " is validated");
        clickByXpath("//input[@type='submit']");
        driver.findElement(By.linkText("Nonfiction")).click();
        //tagname[text()='textThatYouAreExpecting']
        //tagname[contains(text(),'partialTextThatYouAreExpecting')]
        sleepFor(3);
        clickByXpath("//h2[text()='Auction']");
        sleepFor(5);
        quiteBrowser();
    }
}
