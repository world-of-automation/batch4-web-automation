package seleniumbasic;

import core.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SeleniumPractice2 {

    static WebDriver driver = TestBase.getDriver("chrome", "https://www.ebay.com", "mac", false);

    public static void main(String[] args) {

        //driver.findElement(By.id("gh-ac")).sendKeys("Java Books");

        WebElement element = driver.findElement(By.id("gh-ac"));
        element.sendKeys("Java Books");
        System.out.println(element.getAttribute("value"));
        TestBase.sleepFor(5);
        element.clear();
        element.sendKeys("Java Books");

        WebElement element1 = driver.findElement(By.xpath("//input[@type='submit']"));
        System.out.println(element1.getAttribute("value"));
        element1.click();

        WebElement element2 = driver.findElement(By.linkText("Nonfiction"));
        System.out.println(element2.getText());
        System.out.println(element2.isDisplayed());

        WebElement element3 = driver.findElement(By.xpath("//input[@type='checkbox' and @aria-label='Board Book']"));
        System.out.println("before click : " + element3.isSelected());
        element3.click();

        WebElement element4 = driver.findElement(By.xpath("//input[@type='checkbox' and @aria-label='Board Book']"));
        //WebElement element4 = getElementFromXpath("//input[@type='checkbox' and @aria-label='Board Book']");

        System.out.println("after click : " + element4.isSelected());

        TestBase.sleepFor(5);
        driver.quit();
    }


}
