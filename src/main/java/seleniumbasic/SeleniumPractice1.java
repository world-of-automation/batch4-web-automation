package seleniumbasic;

import core.TestBase;
import org.openqa.selenium.WebDriver;

public class SeleniumPractice1 {

    public static void main(String[] args) {
        WebDriver driver = TestBase.getDriver("chrome", "https://www.ebay.com", "mac", false);

        //By.id();
        //driver.findElement(By.id("gh-shop-a")).click();

        //By.xpath();
        //driver.findElement(By.xpath("//button[@id='gh-shop-a']")).click();
        //tagName[@attributeName='attributeValue']

        //driver.findElement(By.xpath("//button[@id='gh-shop-a' and @class='gh-control']")).click();
        //tagName[@attributeName='attributeValue' and @2ndAttributeName='2ndAttributeValue']

        //driver.findElement(By.xpath("(//button[@id='gh-shop-a'])[1]")).click();
        //tagName[@attributeName='attributeValue'])[numberOfTheIndex]

        //By.linkText();
        //driver.findElement(By.linkText("Sign in")).click();

        //By.partialLinkText();
        //driver.findElement(By.partialLinkText("Verified Rights Owner")).click();

        //By.cssSelector();
        //driver.findElement(By.cssSelector("#gh-p-1 > a")).click();

        /*
        By.name();
        By.tagName();
        By.className();*/

        TestBase.sleepFor(5);
        driver.quit();
    }


}
