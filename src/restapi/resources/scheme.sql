drop database if exists worldOfAutomation;

create database worldOfAutomation;

use worldOfAutomation;

DROP TABLE if exists `employee`;

create TABLE `employee`
(
    `id`           int(11) NOT NULL AUTO_INCREMENT,
    `name`         varchar(45) DEFAULT NULL,
    `permanent`    tinyint(1)  DEFAULT NULL,
    `phone_number` int(11)     DEFAULT NULL,
    `role`         varchar(45) DEFAULT NULL,
    `city`         varchar(45) DEFAULT NULL,
    PRIMARY KEY (`id`)
);

DROP TABLE if exists `address`;

CREATE TABLE `address`
(
    `employee_id` int(11) NOT NULL,
    `street`      varchar(45) DEFAULT NULL,
    `apt`         varchar(45) DEFAULT NULL,
    `city`        varchar(45) DEFAULT NULL,
    `zipcode`     int(11)     DEFAULT NULL,
    `state`       varchar(2)  DEFAULT NULL,
    PRIMARY KEY (`employee_id`)
);
