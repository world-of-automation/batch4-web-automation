employees/registration - This returns either the user is registered or not
/employees/authentication - This authenticates the user to access the api
/employees/all - This returns all employees in the Employees table
/employees/{id} - This returns the details of the employee passed in URL
/employees/delete/{id} - Delete's an employee based on the id provided
/employees/update/{id} - Updates's an employee based on the id provided. Have to give the json file with updated data
/employees/add - Add new employee/employees using the Employee model. Have to give the json file with new data eg. `[ { "name": "rohan", "permanent": 1, "phone_number": 3482934, "role": "qa", "city": "NJ" }, { "name": "zan", "permanent": 0, "phone_number": 46453, "role": "dev", "city": "NY" }
/address/all - This returns all address 
/address/{id} - This returns the details of the address passed in URL
/address/delete/{id} - Delete's an address based on the id provided
/address/update/{id} - Updates's an address based on the id provided. Have to give the json file with updated data
/address/add - Add new address/addresses using the address model. Have to give the json file with new data eg. `[ { "street": "6360", "city": "rego park, "state": "NY, "apt": "g23", "zipcode": 11374 } }