- `/users/all` - This returns all Users in the Users table which is created in H2
- `/users/{name}` - This returns the details of the Users passed in URL
- `/users/add` - Add new users using the Users model. Have to give the json file with new data eg. 
`{ "name": "Rohan", "teamName": "Development", "salary": 100000 }`
- `/users/delete/{id}` - Delete's an user based on the id provided
- `/users/update/{id}` - Updates's an user based on the id provided. Have to give the json file with updated data
