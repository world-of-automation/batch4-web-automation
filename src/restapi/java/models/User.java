package models;

public class User {
    private int id, salary;
    private String name, teamName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", salary=" + salary +
                ", name='" + name + '\'' +
                ", teamName='" + teamName + '\'' +
                '}';
    }
}
