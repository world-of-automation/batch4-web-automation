package models;

import lombok.Data;

@Data
public class Employee {
    private String name, role, city;
    private int phone_number, permanent;

    public Employee() {

    }

    public Employee(String name, String role, String city, int phone_number, int permanent) {
        this.name = name;
        this.role = role;
        this.city = city;
        this.phone_number = phone_number;
        this.permanent = permanent;
    }

}
