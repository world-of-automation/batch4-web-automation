package restapitests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import core.ConnectDB;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import models.Employee;
import models.User;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import static io.restassured.RestAssured.given;

public class SQLDatabaseAPITests extends APITestBase {

    @Test
    public void getAllTheEmployees() throws IOException {
        RestAssured.baseURI = "http://localhost:8090/";
        Response response = given().when().log().all().get("/users/all")
                .then().assertThat().statusCode(200).extract().response();

        System.out.println(response.prettyPrint());

        ObjectMapper objectMapper = new ObjectMapper();
        User[] users = objectMapper.readValue(response.asString(), User[].class);
        System.out.println(users.length);
        System.out.println(users[0].getName());
    }

    @Test
    public void addAEmployee() throws JsonProcessingException, SQLException {
        JSONArray js = getJsonArray("src/restapi/resources/UserData2.json");

        JSONObject userObjectFromJsonFIle = (JSONObject) js.get(0);
        System.out.println(userObjectFromJsonFIle);
        ObjectMapper objectMapper = new ObjectMapper();
        Employee employeeForSend = objectMapper.readValue(userObjectFromJsonFIle.toString(), Employee.class);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", "123456789");
        jsonObject.put("phone_number", employeeForSend.getPhone_number());
        jsonObject.put("city", employeeForSend.getCity());
        jsonObject.put("role", employeeForSend.getRole());
        jsonObject.put("permanent", employeeForSend.getPermanent());

        System.out.println(jsonObject);

        JSONArray jsonArray = new JSONArray();
        jsonArray.add(jsonObject);

        RestAssured.baseURI = "http://localhost:8090";

        Response response = given().header("Content-Type", "application/json")
                .body(jsonArray).when().log().all().post("/employees/add")
                .then().assertThat().statusCode(200).extract().response();

        System.out.println(response.asString());


        Connection connection = ConnectDB.getConnection("root", "root1234", "worldOfAutomation");

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from worldOfAutomation.employee where name ='" + employeeForSend.getName() + "';");

        String name, role, city;
        int phone_number, permanent;
        ArrayList<Employee> employeesToGet = new ArrayList<>();
        while (resultSet.next()) {
            name = resultSet.getString("name");
            role = resultSet.getString("role");
            city = resultSet.getString("city");
            phone_number = resultSet.getInt("phone_number");
            permanent = resultSet.getInt("permanent");

            Employee employee2 = new Employee(name, role, city, phone_number, permanent);
            employeesToGet.add(employee2);
        }

        System.out.println(employeesToGet);

        Assert.assertEquals(employeeForSend, employeesToGet.get(0));

    }


    @Test
    public void deleteSpecificUser() throws SQLException {
        RestAssured.baseURI = "http://localhost:8090/";
        Response response = given().when().log().all().delete("/employees/delete/7")
                .then().assertThat().statusCode(204).extract().response();

        System.out.println(response.asString());

        Connection connection = ConnectDB.getConnection("root", "root1234", "worldOfAutomation");

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from worldOfAutomation.employee where id =7;");

        Assert.assertFalse(resultSet.next());

    }

}
