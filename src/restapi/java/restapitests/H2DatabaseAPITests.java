package restapitests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import models.User;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import java.io.IOException;

import static io.restassured.RestAssured.given;

public class H2DatabaseAPITests extends APITestBase {


    @Test
    public void getAllTheUsersH2() throws IOException {
        RestAssured.baseURI = "http://localhost:8083/";
        Response response = given().when().log().all().get("/users/all")
                .then().assertThat().statusCode(200).extract().response();

        System.out.println(response.prettyPrint());

        ObjectMapper objectMapper = new ObjectMapper();
        User[] users = objectMapper.readValue(response.asString(), User[].class);
        System.out.println(users.length);
        System.out.println(users[0].getName());
    }

    @Test
    public void addAUserH2() throws JsonProcessingException {

        JSONArray js = getJsonArray("src/restapi/resources/UserData.json");

        JSONObject userObjectFromJsonFIle = (JSONObject) js.get(0);
        System.out.println(js);

        ObjectMapper objectMapper = new ObjectMapper();
        User user = objectMapper.readValue(userObjectFromJsonFIle.toJSONString(), User.class);


        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", user.getName());
        jsonObject.put("salary", user.getSalary());
        jsonObject.put("teamName", user.getTeamName());
        System.out.println(user.getName());


        System.out.println(userObjectFromJsonFIle);
        RestAssured.baseURI = "http://localhost:8083";

        Response response = given().header("Content-Type", "application/json")
                .body(jsonObject).when().log().all().post("/users/add")
                .then().assertThat().statusCode(201).extract().response();

        System.out.println(response.asString());
    }


    @Test
    public void deleteSpecificUser() {
        RestAssured.baseURI = "http://localhost:8083/";
        Response response = given().when().log().all().delete("/users/delete/10")
                .then().assertThat().statusCode(204).extract().response();

        System.out.println(response.asString());
    }

    @Test
    public void updateSpecificUser() throws JsonProcessingException {
        JSONArray js = getJsonArray("src/restapi/resources/UserData.json");
        JSONObject userObjectFromJsonFIle = (JSONObject) js.get(0);
        ObjectMapper objectMapper = new ObjectMapper();
        User user = objectMapper.readValue(userObjectFromJsonFIle.toJSONString(), User.class);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", user.getName());
        jsonObject.put("salary", 200000);
        jsonObject.put("teamName", user.getTeamName());

        RestAssured.baseURI = "http://localhost:8083";

        Response response = given().header("Content-Type", "application/json")
                .body(jsonObject).when().log().all().put("/users/update/9")
                .then().assertThat().statusCode(204).extract().response();

        System.out.println(response.asString());
    }
}
