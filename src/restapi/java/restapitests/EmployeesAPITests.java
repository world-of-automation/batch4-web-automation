package restapitests;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class EmployeesAPITests {


    @Test
    public void testAddingUser() {
        RestAssured.baseURI = "http://localhost:8080/employees";

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", "Sourav");
        jsonObject.put("phone_number", 5678);
        jsonObject.put("role", "lead");
        jsonObject.put("permanent", true);
        jsonObject.put("city", "NY");


        JSONObject jsonObject2 = new JSONObject();
        jsonObject2.put("name", "Rahman");
        jsonObject2.put("phone_number", 56789);
        jsonObject2.put("role", "QA");
        jsonObject2.put("permanent", false);
        jsonObject2.put("city", "NJ");


        jsonArray.add(jsonObject);
        jsonArray.add(jsonObject2);

        Response response = given().header("Content-Type", "application/json")
                .body(jsonArray.toString())
                .when().log().all().post("/add")
                .then().assertThat().statusCode(200).extract().response();
    }


    @Test
    public void getAllUsers() {
        RestAssured.baseURI = "http://localhost:8080/employees";

        Response response = given()
                .when().log().all().get("/all")
                .then().assertThat().statusCode(200).extract().response();

        System.out.println(response.body().prettyPrint());
    }


}
