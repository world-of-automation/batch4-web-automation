package restapitests;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

public class ReadJSON extends APITestBase {


    @Test
    public void readSingleArray_SingleJson() {
        JSONArray js = getJsonArray("src/restapi/resources/SingleArray_SingleJson.json");

        System.out.println(js);
        JSONObject jsonObject = (JSONObject) js.get(0);
        System.out.println(jsonObject);

        jsonObject.put("feature", "new");
        jsonObject.put("time", "EST");

        System.out.println(jsonObject);
        System.out.println(js);
    }


    @Test
    public void readSingleArray_MultipleJson() {
        JSONArray js = getJsonArray("src/restapi/resources/SingleArray_MultipleJson.json");

        System.out.println(js);
        JSONObject jsonObject = (JSONObject) js.get(0);
        System.out.println(jsonObject);

        JSONObject jsonObject1 = (JSONObject) js.get(1);
        System.out.println(jsonObject1);

    }


    @Test
    public void readSingleArray_JsonInMultipleJson() {
        JSONArray js = getJsonArray("src/restapi/resources/SingleArray_JsonInMultipleJson.json");

        System.out.println(js);
        JSONObject jsonObject = (JSONObject) js.get(0);
        System.out.println(jsonObject);

        JSONObject dataJson = (JSONObject) jsonObject.get("data");
        dataJson.put("name", "opu");
        System.out.println(dataJson);


        JSONObject jsonObject1 = (JSONObject) js.get(1);
        System.out.println(jsonObject1);

    }


    @Test
    public void readSingleArray_ArrayInMultipleJson() {
        JSONArray js = getJsonArray("src/restapi/resources/SingleArray_ArrayInMultipleJson.json");

        System.out.println(js);
        JSONObject jsonObject = (JSONObject) js.get(0);
        System.out.println(jsonObject);

        JSONArray dataJson = (JSONArray) jsonObject.get("data");
        JSONObject naimObj = (JSONObject) dataJson.get(1);
        System.out.println(naimObj);


        JSONObject jsonObject1 = (JSONObject) js.get(1);
        System.out.println(jsonObject1);

    }


}
