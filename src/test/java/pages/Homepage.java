package pages;

import core.ExtentTestManager;
import core.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static core.ConnectDB.statement;

public class Homepage {

    @FindBy(xpath = "//input[@id='gh-ac']")
    private WebElement searchBar;

    @FindBy(xpath = "//input[@type='submit']")
    private WebElement searchBtn;

    @FindBy(linkText = "Sign in")
    private WebElement signInBtn;

    @FindBy(xpath = "//select[@id='gh-cat']/option")
    private List<WebElement> allCategoriesList;

    @FindBy(xpath = "//select[@id='gh-cat']")
    private WebElement allCategories;

    @FindBy(linkText = "Fashion")
    private WebElement fashion;

    @FindBy(linkText = "Watches")
    private WebElement watches;

    public void clickOnSignInButton() {
        signInBtn.click();
    }

    public void typeOfSearchBar() {
        searchBar.sendKeys("Java Books");
    }

    public void typeOfSearchBarDataFromDB() throws SQLException {
        String query = "select book_name from datas;";
        ResultSet table = statement.executeQuery(query);
        ArrayList<String> books = new ArrayList<String>();
        while (table.next()) {
            books.add(table.getString("book_name"));
        }

        for (int i = 0; i < books.size(); i++) {
            searchBar.sendKeys(books.get(i));
            clickOnSearchButton();
            searchBar.clear();
        }
    }

    public void clickOnSearchButton() {
        searchBtn.click();
    }

    public void selectADropdown(String dropdownName) {
        TestBase.waitForElementToBeVisible(allCategories);
        allCategories.click();
        ExtentTestManager.log("clicked on all categoris");
        Select select = new Select(allCategories);
        //select.selectByVisibleText();
        //select.selectByIndex();
        //select.selectByValue();
        select.selectByVisibleText(dropdownName);
        ExtentTestManager.log("selected dropdown : " + dropdownName);
        TestBase.sleepFor(5);
    }

    public void getTheDropdownDataFromCategories() {
        int numberOfCat = allCategoriesList.size();
        ExtentTestManager.log("getting all categories size : " + numberOfCat);
        Assert.assertEquals(numberOfCat, 36);
        for (int i = 0; i < allCategoriesList.size(); i++) {
            ExtentTestManager.log(allCategoriesList.get(i).getText());
        }
    }

    public void mouseHoverForEbayHomePagw() {
        TestBase.mouseHoverTo(fashion, watches);
        ExtentTestManager.captureScreenshot(TestBase.driver, "test_screenshot.png");
    }


    public void validateSearchIsWorking() {
        //something
    }


}
