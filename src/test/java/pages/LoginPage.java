package pages;

import core.TestBase;
import org.testng.Assert;

public class LoginPage extends TestBase {

    public void validateLoginPageIsDisplayed() {
        sleepFor(20);
        String currentTitle = driver.getTitle();
        Assert.assertEquals(currentTitle, "");
    }


}
