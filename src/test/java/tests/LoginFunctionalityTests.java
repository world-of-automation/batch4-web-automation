package tests;

import core.TestBase;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import pages.Homepage;
import pages.LoginPage;

public class LoginFunctionalityTests extends TestBase {

    @Test(groups = {"regression"})
    public void validateUserWithRightCredentialsBeingAbleToLogin() {
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        Homepage homepage = PageFactory.initElements(driver, Homepage.class);
        homepage.clickOnSignInButton();
        loginPage.validateLoginPageIsDisplayed();

    }
}
