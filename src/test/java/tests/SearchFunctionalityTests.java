package tests;

import core.ConnectDB;
import core.TestBase;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.Homepage;

import java.sql.SQLException;

import static core.ConnectDB.getConnection;
import static core.Utilities.getPropertyFromConfig;

public class SearchFunctionalityTests extends TestBase {

    @BeforeClass(alwaysRun = true)
    public static void setupDB() throws SQLException {
        ConnectDB.connection = getConnection(getPropertyFromConfig("dbUsername"), getPropertyFromConfig("dbPassword"), getPropertyFromConfig("dbName"));
        ConnectDB.statement = ConnectDB.connection.createStatement();
    }

    @Test(enabled = true, groups = {"smoke"})
    public void validateUserBeingAbleToSearch() {
        Homepage homepage = PageFactory.initElements(driver, Homepage.class);
        homepage.typeOfSearchBar();
        homepage.clickOnSearchButton();
    }

    @Test
    public void validateUserBeingAbleToSearchUsingDBData() throws SQLException {
        Homepage homepage = PageFactory.initElements(driver, Homepage.class);
        homepage.typeOfSearchBarDataFromDB();
    }


}
