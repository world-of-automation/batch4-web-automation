package tests;

import core.TestBase;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.Homepage;

public class HomePageValidationTests extends TestBase {

    @DataProvider(name = "getDropdowns")
    public static Object[] getDropdowns() {
        return new Object[]{"Baby", "Art"};
    }

    @DataProvider(name = "getCredentials")
    public static Object[][] getCredentials() {
        return new Object[][]{{"user001", "pass001"}, {"user002", "pass002"}};
    }

    @Test(enabled = true, groups = {"smoke"})
    public void validateAllCategoriesDropDown() {
        Homepage homepage = PageFactory.initElements(driver, Homepage.class);
        homepage.getTheDropdownDataFromCategories();
    }


    //********************

    @Test(enabled = true)
    public void selectArtFromTheCategoriesDropdown() {
        Homepage homepage = PageFactory.initElements(driver, Homepage.class);
        homepage.selectADropdown("Baby");
    }

    @Test(enabled = true)
    public void validateUserBeingAbleToSelectWatchesFromFashion() {
        Homepage homepage = PageFactory.initElements(driver, Homepage.class);
        homepage.mouseHoverForEbayHomePagw();
    }

    @Test(dataProvider = "getDropdowns", groups = {"smoke"})
    public void selectFromTheCategoriesDropdownUsingDataProvider(String dropdownName) {
        Homepage homepage = PageFactory.initElements(driver, Homepage.class);
        homepage.selectADropdown(dropdownName);
    }

    @Test(dataProvider = "getCredentials", enabled = false, groups = {"regression"})
    public void loginToFacevook(String username, String password) {
        System.out.println(username);
        System.out.println(password);
    }

}
