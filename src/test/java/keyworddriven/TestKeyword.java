package keyworddriven;

import org.testng.annotations.Test;

public class TestKeyword extends EventCase {

    @Test
    public void validateUserWithRightCredentialsBeingAbleToLogin() {
        functionEventCase(functionNames.clickSignIn);
        functionEventCase(functionNames.validateLoginPage);
        functionEventCase(functionNames.validateSearchIsWorking);
    }


}
