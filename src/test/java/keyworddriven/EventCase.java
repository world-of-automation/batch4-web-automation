package keyworddriven;

import core.TestBase;
import org.openqa.selenium.support.PageFactory;
import pages.Homepage;
import pages.LoginPage;

public class EventCase extends TestBase {

    public void functionEventCase(functionNames functionName) {
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        Homepage homepage = PageFactory.initElements(driver, Homepage.class);
        switch (functionName) {
            case clickSignIn:
                homepage.clickOnSignInButton();
                break;
            case validateLoginPage:
                loginPage.validateLoginPageIsDisplayed();
                break;
            case validateSearchIsWorking:
                homepage.validateSearchIsWorking();
        }
    }

    public enum functionNames {
        clickSignIn, validateLoginPage, validateSearchIsWorking
    }
}
