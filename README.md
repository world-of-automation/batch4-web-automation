**Steps to run the project locally :** 
This project's credentials should be in the src/test/resources/config.properties file
Provide the credentials accordingly in order to run the tests




**FYI :** 
Double click & Right click : http://demo.guru99.com/test/simple_context_menu.html

iFrame : https://demoqa.com/iframe-practice-page

DragNdrop : http://demo.guru99.com/test/drag_drop.html

MouseHover, Dropdown, Scroll: https://www.ebay.com

Alert : http://demo.guru99.com/test/delete_customer.php

Radio Button : https://www.aa.com